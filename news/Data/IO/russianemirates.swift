//
//  russianemirates.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import Moya
import SwiftUI

public enum RussianEmirates {

  static private let publicKey = "YOUR PUBLIC KEY"
  static private let privateKey = "YOUR PRIVATE KEY"

  case rubric
  case news(TimeInterval, TimeInterval, String)
}

extension RussianEmirates: TargetType {
  
  public var baseURL: URL {
    return URL(string: "https://russianemirates.com/api")!
  }

  public var path: String {
    switch self {
      case .rubric: return "/rubric/"
      case .news: return "/news/"
    }
  }
  
  public var method: Moya.Method {
    switch self {
      case .rubric: return .get
      case .news: return .post
    }
  }
  
  public var sampleData: Data {
    return Data()
  }
  
  public var task: Task {
    
    switch self {
    case .rubric:
      return .requestParameters(parameters: [:], encoding: URLEncoding.default)
    case .news(let date_from, let date_to, let category):
      
      let date_from_Data = String(date_from).data(using: String.Encoding.utf8) ?? Data()
      let date_to_Data = String(date_to).data(using: String.Encoding.utf8) ?? Data()
      let category_Data = category.data(using: String.Encoding.utf8) ?? Data()
      
      let formData: [Moya.MultipartFormData] = [Moya.MultipartFormData(provider: .data(date_from_Data), name: "date_from"),
                                                Moya.MultipartFormData(provider: .data(date_to_Data), name: "date_to"),
                                                Moya.MultipartFormData(provider: .data(category_Data), name: "category")
      ]
      return .uploadMultipart(formData)
    }
    
  }
  
  public var headers: [String: String]? {
        
    switch self {
    case .rubric:
      return ["Content-Type": "application/json"]
    case .news:
      return ["Content-type" : "multipart/form-data"]
    }
    
  }
  
  public var validationType: ValidationType {
    return .successCodes
  }
  
}

