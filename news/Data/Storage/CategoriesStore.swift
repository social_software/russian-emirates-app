//
//  CategoriesStore.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import Moya

enum CategoryType: String {
  
  case Latest = "Latest"
  case World = "World"
  case Business = "Business"
  case Sports = "Sports"
  case Life = "Life"
  case Technology = "Technology"
  case Art = "Art"

}

class CategoriesStore: ObservableObject {
  
  @Published var list: Categories
  @Published var status: NetworkStatus? = nil
  
  init() {
    list = Categories()
    self.update()
  }
  
  public func clear(){
    self.list.removeAll()
  }
  
  public func update() {
    
    self.status?.completed = false
    
    let provider = MoyaProvider<RussianEmirates>()
    provider.request(.rubric) { result in
      
      self.status?.completed = false
      self.status?.result = result
      
      switch result {
      case .success(let response):

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        
        do {
        
          let filteredResponse = try response.filterSuccessfulStatusCodes()
          let categoriesData:CategoriesRaw = try filteredResponse.map(CategoriesRaw.self)
          
          // transformation to clean data
          let categories: Categories = categoriesData.map{ (raw) -> Category in
            
            return Category(id: raw.id, name: raw.name, parentId: raw.parentId, parent: nil)
          }.sorted(by: { $0.name.compare($1.name) == .orderedAscending })
          
          // fix parent relationship
//          categories.forEach{ $0.findParent(categories) }
          
          DispatchQueue.main.async {
            self.list = categories
            self.status?.completed = true
          }
          
        } catch {
            print(error.localizedDescription)
        }
        
      case .failure(let error):
        debugPrint("error: ", error)
      }
      
    }
    
  }
  
}

