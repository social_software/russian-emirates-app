//
//  CategoriesData.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import Foundation
import Moya

class CategoriesData {
  static let shared = CategoriesData()
  
  var list: Categories = Categories()
  public var categories: Categories { get { return list }}
  
  init(){
    self.list = update()
    debugPrint(list)
  }
  
  private func load() -> Categories {
    
    var categories: Categories? = nil
    
    let provider = MoyaProvider<RussianEmirates>()
    provider.request(.rubric) { result in
      
      switch result {
      case .success(let response):

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        
        do {
          let filteredResponse = try response.filterSuccessfulStatusCodes()
          let categoriesData:CategoriesRaw = try filteredResponse.map(CategoriesRaw.self)
          
          // transformation to clean data
          categories = categoriesData.map{ (raw) -> Category in
            return Category(id: raw.id, name: raw.name, parentId: raw.parentId, parent: nil)
          }.sorted(by: { $0.name.compare($1.name) == .orderedAscending })
          
          // fix parent relationship
//          categories.forEach{ $0.findParent(categories) }
                    
        } catch {
            print(error.localizedDescription)
        }
        
      case .failure(let error):
        debugPrint("error: ", error)
      }
      
    }
    
    return categories ?? Categories()
  }
  
  public func update() -> Categories {
    // TODO: load stored data
    return self.load()
  }
  
}

