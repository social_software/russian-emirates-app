//
//  ArticlesStore.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import Moya

class NetworkStatus {
  public var result: Result<Response, MoyaError>? = nil
  public var completed: Bool = false
}

class ArticlesStore: ObservableObject {
  @Published var list: Articles
  @Published var status: NetworkStatus? = nil
  
  private var categories: Categories
  private var articlesType: newsType = .news
  
  init(categories: Categories, channel: newsType? = .news) {
    list = Articles()
    
    self.categories = categories
    self.articlesType = channel ?? .news
    self.status = NetworkStatus()
    self.recent(channel: self.articlesType)
  }
  
  public func clear(){
    self.list.removeAll()
  }
  
  public func recent(channel: newsType? = nil) {
    if let channel = channel {
      self.recent(channel: channel)
    } else { self.recent(channel: self.articlesType) }
  }
  
  public func recent(channel: newsType){
    
    let date_from:TimeInterval = Calendar.current.date(byAdding: .day, value: -5, to: Date())!.timeIntervalSince1970
    let date_to:TimeInterval =  Date().timeIntervalSince1970
//    let channel: String = (channel ?? .headLines)!.rawValue
    let channel: String = channel.rawValue
    
    self.status?.completed = false
    
    let provider = MoyaProvider<RussianEmirates>()
    provider.request(.news(date_from, date_to, channel)) { result in
      
      self.status?.completed = false
      self.status?.result = result
      
      switch result {
      case .success(let response):
                
        do {
        
          let filteredResponse = try response.filterSuccessfulStatusCodes()
          let articlesRawData:ArticlesRaw = try filteredResponse.map(ArticlesRaw.self)
                    
          // transformation to clean data
          let articles: Articles = articlesRawData.map{ (key: String, value: ArticleRaw) -> Article in
            
            let id:Int = Int(key) ?? 0
            let publishedDate = Date(timeIntervalSince1970: Double(value.published))
            let tags: [String] = value.tags.components(separatedBy: ", ")
            
            let absoluteUrl: URL? = URL(string: value.absoluteUrl)
            let imageUrl: URL? = URL(string: value.imageUrl)
            
//            let categories = value.categoryCodes.compactMap{ (code) -> Category? in
//              return self.categories.first(where: { $0.id == code })
//            }
            
            return Article(id: id, titleText: value.titleText, previewText: value.previewText, imageUrl: imageUrl, absoluteUrl: absoluteUrl, published: publishedDate, viewCount: value.viewCount, tags: tags, categories: [])
          }.sorted(by: { $0.published.compare($1.published) == .orderedDescending })
                    
          DispatchQueue.main.async {
            self.list.append(contentsOf: articles)
            self.status?.completed = true
          }
          
        } catch {
            debugPrint(error.localizedDescription)
        }
        
      case .failure(let error):
        debugPrint("error: ", error)
      }
    }
  }

//  public func download( _ onReceived: (( Any? ) -> Void)?) {
//
//
//    let date_from = Calendar.current.date(byAdding: .day, value: -5, to: Date())!.timeIntervalSince1970
//    let date_to =  Date().timeIntervalSince1970
//    let category = "uae-news"
//
//    let provider = MoyaProvider<RussianEmirates>()
//    provider.request(.news(date_from, date_to, category)) { result in
//
//      switch result {
//      case .success(let response):
//
//        let decoder = JSONDecoder()
//        decoder.keyDecodingStrategy = .convertFromSnakeCase
//        decoder.dateDecodingStrategy = .secondsSince1970
//
//        do {
//
//          let filteredResponse = try response.filterSuccessfulStatusCodes()
//          let articlesRawData:ArticlesRaw = try filteredResponse.map(ArticlesRaw.self)
//
////          DispatchQueue.main.async {
////              self.list = categoriesData
////          }
//
//        } catch {
//            debugPrint(error.localizedDescription)
//        }
//
//      case .failure(let error):
//        debugPrint("error: ", error)
//      }
//
//    }
//
//  }

}

