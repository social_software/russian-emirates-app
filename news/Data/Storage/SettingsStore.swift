//
//  SettingsStore.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import CloudKit

final class SettingsStore: ObservableObject {
  @Published var defaults: UserDefaults
  
  public enum settingsCode: String {
    case userName = "user.preferences.name"
    case userEmail = "user.preferences.email"
  }
  
  init(defaults: UserDefaults = .standard) {
    self.defaults = defaults
    
    defaults.register(defaults: [
      settingsCode.userName.rawValue: "Unknown",
      settingsCode.userEmail.rawValue: "Unknown"
      
    ])
    
  }
  
  var userName: String {
    get {
      defaults.string(forKey: settingsCode.userName.rawValue) ?? "Unknown"
    }
    set {
      defaults.set(newValue, forKey: settingsCode.userName.rawValue)
    }
  }
  
  var userEmail: String {
    get {
      defaults.string(forKey: settingsCode.userEmail.rawValue) ?? "Unknown"
    }
    set {
      defaults.set(newValue, forKey: settingsCode.userEmail.rawValue)
    }
  }
  
  public func readCloudProfileData(_ onReceived: (( Any? ) -> Void)?) {
    
    // reading CloudKit user information
    let container = CKContainer.default()
    container.fetchUserRecordID(
        completionHandler: { (recordID, error) in
          guard let recordID = recordID else { return }
          container.requestApplicationPermission(.userDiscoverability, completionHandler: { (status, error2) in
            if (status == CKContainer_Application_PermissionStatus.granted) {
              container.discoverUserIdentity(withUserRecordID: recordID, completionHandler: { (info, error3) in
                guard let info = info else { return }

                // get user name string
                let userName = [info.nameComponents?.givenName, info.nameComponents?.middleName, info.nameComponents?.familyName].compactMap{ $0 }.reduce("", { a, b in "\(a) \(b)" }).trimmingCharacters(in: .whitespaces)
                
                // TODO: link to Contacts Database
//                let userContactIds = info.contactIdentifiers
                
                // caching in settings
                self.userName = userName
                
                // callback
                if let onReceived = onReceived {
                  
                  onReceived(userName)
                }
                
              })
            }
          })
        }
    )
    
    

  }
  
}

