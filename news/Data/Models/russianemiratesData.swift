//
//  russianemiratesData.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import Foundation

enum newsType: String {
  case headLines = "uae-headlines"
  case news = "uae-news"
  case property = "uae-property-news"
}

// Categories RAW Data

struct CategoryRaw : Codable {
  
  let id: Int
  let name: String
  let parentId: Int?

  enum CodingKeys: String, CodingKey {
      case id, name
      case parentId = "parent_id"
  }
  
}

typealias CategoriesRaw = [CategoryRaw]

struct Category: Hashable, Identifiable {
  
  let id: Int
  let name: String
  private let parentId: Int?
  let parent: Any?
  
  init(id: Int, name: String, parentId: Int?, parent: Any?){
    self.id = id
    self.name = name
    self.parentId = parentId
    self.parent = parent
  }
  
  static func == (lhs: Category, rhs: Category) -> Bool {
    return lhs.id == rhs.id && lhs.name == rhs.name
  }
  
  func hash(into hasher: inout Hasher) {
      hasher.combine(id)
  }
  
  public func findParent(_ list: Categories) -> Category? {
    return list.first(where: { $0.parentId == self.parentId })
  }
  
}
typealias Categories = [Category]

// Articles RAW Data

struct ArticleRaw: Codable {
  
  let titleText: String
  let previewText: String
  let imageUrl: String
  let absoluteUrl: String
  let published: Int
  let viewCount: Int
  let tags: String
  let categoryCodes: [Int]
  
  enum CodingKeys: String, CodingKey {
    case titleText = "title"
    case previewText = "preview"
    case imageUrl = "img"
    case absoluteUrl = "url"
    case published = "date"
    case viewCount = "views"
    case tags = "tags"
    case categoryCodes = "rubric"
  }
  
}

typealias ArticlesRaw = [String: ArticleRaw]

struct Article: Hashable, Identifiable {
  
  let id: Int
  let titleText: String
  let previewText: String
  let imageUrl: URL?
  let absoluteUrl: URL?
  let published: Date
  let viewCount: Int
  let tags: [String]
  let categories: Categories
  
  init(id: Int, titleText: String, previewText: String, imageUrl: URL?, absoluteUrl: URL?, published: Date, viewCount: Int, tags: [String], categories: [Category]){
    self.id = id
    self.titleText = titleText
    self.previewText = previewText
    self.imageUrl = imageUrl
    self.absoluteUrl = absoluteUrl
    self.published = published
    self.viewCount = viewCount
    self.tags = tags
    self.categories = categories
  }
  
  static func == (lhs: Article, rhs: Article) -> Bool {
    return lhs.id == rhs.id && lhs.titleText == rhs.titleText
  }
  
  func hash(into hasher: inout Hasher) {
      hasher.combine(id)
  }
  
}
typealias Articles = [Article]

