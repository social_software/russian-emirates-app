//
//  ArticleViewer.swift
//  news
//
//  Created by Alex Sporyk on 17/03/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import WebKit
import SwiftUI

struct ArticleViewer : View {
  
  let article: Article
  
  var body: some View {
    
    WebView(request: URLRequest(url: URL(string: article.absoluteUrl?.absoluteString ?? AppTexts.companyLink)!, cachePolicy: .reloadRevalidatingCacheData, timeoutInterval: 120))
      .edgesIgnoringSafeArea(.bottom)
      .edgesIgnoringSafeArea(.leading)
      .edgesIgnoringSafeArea(.trailing)
      
    .navigationBarTitle(Text(article.titleText), displayMode: .inline)
  }
  
}

struct ArticleViewer_Previews: PreviewProvider {
    static var previews: some View {
      ArticleViewer(article: Article(id: 0, titleText: "Russian Emirates", previewText: "News", imageUrl: nil, absoluteUrl: URL(string: AppTexts.companyLink), published: Date(), viewCount: 0, tags: [], categories: []))
    }
}

struct WebView : UIViewRepresentable {
  
  let request: URLRequest
  
  func makeUIView(context: UIViewRepresentableContext<WebView>) -> WKWebView {
    return WKWebView()
  }
  
  func updateUIView(_ uiView: WKWebView, context: Context) {
    uiView.load(request)
  }
  
}
