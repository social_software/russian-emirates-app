//
//  ProfileMainView.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
  @EnvironmentObject var settingsStore: SettingsStore
  
  init() {
    UITableView.appearance().backgroundColor = .clear
    // To remove only extra separators below the list:
    UITableView.appearance().tableFooterView = UIView()
    // To remove only extra space above the list:
    UITableView.appearance().tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: Double.leastNonzeroMagnitude))
  }
  
  var body: some View {
        
    ZStack {
      Color(.systemBackground).edgesIgnoringSafeArea(.all)
      List {
        Section {
          ProfileViewHeaderRow(profileName: "\(settingsStore.userName)", profileEmail: "\(settingsStore.userEmail)")
        }
        Section {
          NavigationLink(destination: EmptyView()) { ProfileViewSimpleRow(rowTitle: "Bookmarks", rowSubTitle: "12") }
          NavigationLink(destination: EmptyView()) { ProfileViewSimpleRow(rowTitle: "Popular Categories", rowSubTitle: "24") }
        }
        Section {
          NavigationLink(destination: EmptyView()) { ProfileViewSimpleRow(rowTitle: "Newsletter") }
          // TODO: create actual settings screen
          NavigationLink(destination: EmptyView()) { ProfileViewSimpleRow(rowTitle: "Settings") }
        }
      }
      .listStyle(GroupedListStyle())
      .background(Color(.systemGroupedBackground))
    }
    .onAppear{
      self.settingsStore.readCloudProfileData({ _ in })
    }
    
  }
  
}

struct ProfileViewHeaderRow: View {
  var profileName: String
  var profileEmail: String
  var profileImage: Image?
  
  var body: some View {
    VStack(spacing: 10) {
      HStack {
        Spacer()
      }
      
      HStack {
        Spacer(minLength: 20.0)
        
        if (self.profileImage != nil) {
          self.profileImage!
            .resizable()
            .clipShape(Circle())
            .shadow(radius: 10)
            .overlay(Circle().stroke(Color.white, lineWidth: 10))
        } else {
          Image("SplashScreenLogo").resizable()
  //        .frame(width: 88.0, height: 88.0)
            .clipShape(Circle())
            .shadow(radius: 10)
            .overlay(Circle().stroke(Color.white, lineWidth: 10))
        }
        
        Spacer(minLength: 20.0)
      }
        
      Text(profileName).font(.title)
      Text(profileEmail).font(.caption)
      
      Button(action: {
        debugPrint("tap on get premium")
      }) {
        HStack {
          Spacer()
          Text("Get Premium - $14.99")
          Spacer()
        }
      }
      .padding(10)
      .font(AppFonts.settingsListTitle)
      .background(Color(AppColors.buttonBackground))
      .foregroundColor(Color.white)
      .cornerRadius(10)
      
      Spacer(minLength: 10)
      
    }
    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading)

  }
  
}

struct ProfileViewSimpleRow: View {
  var rowTitle: String
  var rowSubTitle: String?
  
  var body: some View {
    HStack(spacing: 10) {
      Text(rowTitle).font(AppFonts.settingsListTitle).foregroundColor(Color(AppColors.gray90))
      Spacer()
      rowSubTitle.map{ Text($0).font(AppFonts.settingsListSubTitle).foregroundColor(Color(AppColors.gray90)) }
      
    }.padding(.vertical, 10)
  }
  
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
      ProfileView().environmentObject(SettingsStore())
    }
}

