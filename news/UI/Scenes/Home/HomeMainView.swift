//
//  HomeMainView.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import SwiftUI
import Moya

struct HomeView: View {
  
  @EnvironmentObject var headlineNewsStore: ArticlesStore
//  @EnvironmentObject var newsStore: ArticlesStore
//  @EnvironmentObject var businessNewsStore: ArticlesStore
  
  @Binding var showSearch: Bool
  
  @State var isRefreshing: Bool = false
  @State var pullStatus: CGFloat = 0.0
    
  init(showSearch: Binding<Bool>) {
    _showSearch = showSearch
    
    UITableView.appearance().backgroundColor = .clear
    // To remove only extra separators below the list:
    UITableView.appearance().tableFooterView = UIView()
    // To remove only extra space above the list:
    UITableView.appearance().tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: Double.leastNonzeroMagnitude))
    // To remove all separators including the actual ones:
    UITableView.appearance().separatorStyle = .none
  }
  
  var body: some View {
    GeometryReader { geometry in
      ZStack {
        Color(.systemBackground).edgesIgnoringSafeArea(.all)
        RefreshableList(showRefreshView: self.$isRefreshing, pullStatus: self.$pullStatus, action: {
          self.headlineNewsStore.clear()
          DispatchQueue.main.async {
            self.headlineNewsStore.recent()
          }
        }) {
                              
          if(!(self.headlineNewsStore.status?.completed ?? false)) {
            LoadingScreen()
              .frame(width: geometry.size.width, height: geometry.size.height)
          } else {
            
            if(self.showSearch) {
              SearchBar()
            }
            
            if(self.headlineNewsStore.status?.completed ?? false) {
              CategoryFilter()
            }
            
            if !self.headlineNewsStore.list.isEmpty {
              headlinesNewsSection(geometry: geometry)
            }
            
            
            // news : card list
            
            
            // business news : [card horizontally]
            
            // other : card list
            
            
            if(self.headlineNewsStore.status?.completed ?? false) {
              NewsletterForm()
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 250, maxHeight: 250)
            }

          }
        }
        .padding(.leading, -18)
        .padding(.trailing, -18)
      }
    }
    
  }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
      HomeView(showSearch: .constant(false))
        .environmentObject(ArticlesStore(categories: Categories(), channel: .headLines))
        .environmentObject(ArticlesStore(categories: Categories(), channel: .news))
        .environmentObject(ArticlesStore(categories: Categories(), channel: .property))
    }
}

struct SearchBar: UIViewRepresentable {
  
  let controller = UISearchController()
  
  func makeUIView(context: UIViewRepresentableContext<SearchBar>) -> UISearchBar {
      self.controller.searchBar
  }

  func updateUIView(_ uiView: UISearchBar, context: UIViewRepresentableContext<SearchBar>) {

  }

  typealias UIViewType = UISearchBar

}
