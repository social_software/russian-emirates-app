//
//  HeadlineNewsSection.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import SwiftUI

struct headlinesNewsSection : View {
  
  @EnvironmentObject var headlineNewsStore: ArticlesStore
  
  let geometry: GeometryProxy
  
  var body : some View {
    
    ZStack {
      Color(AppColors.systemDefaultBackgroundColor).edgesIgnoringSafeArea(.all)
      
      ScrollView(.horizontal, showsIndicators: false, content: {
        HStack(spacing: 10) {
          ForEach(headlineNewsStore.list, id: \.self) { article in
            NavigationLink(destination: ArticleViewer(article: article)) {
              headlineCard(article: article, geometry: self.geometry)
            }
            .buttonStyle(PlainButtonStyle())
          }
          Spacer(minLength: 10)
        }
        .padding(.leading, 20)
      })
      .frame(height: geometry.size.height/2 + 100)
    }
      
  }
  
}

struct headlineCard : View {
  
  @Environment(\.colorScheme) var colorScheme: ColorScheme
  
  let article: Article
  let geometry: GeometryProxy
  
  let formatter:RelativeDateTimeFormatter = {
    let format = RelativeDateTimeFormatter()
    format.dateTimeStyle = .named
    format.unitsStyle = .short
    return format
  }()
  
  
  fileprivate func remoteImage() -> some View {
      RemoteImage(imageURL: article.imageUrl!, placeholderImage: UIImage(named: "SplashScreenLogo")!)
        .frame(width: geometry.size.width - 40, height: geometry.size.height/2 - 20, alignment: .top)
        .clipped(antialiased: true)
        .border(Color.gray.opacity(0.5), width: 0.5)
        .cornerRadius(8)
  }
  
  fileprivate func metaInformation() -> some View {
    
    HStack(spacing: 10) {

      // TODO: redo it with internal categories
      ForEach(article.categories, id: \.self) { category in
        Text(category.name)
          .foregroundColor(Color(AppColors.buttonBackground))
      }
      
      Text("\u{2022}")
        .foregroundColor(Color(colorScheme == .light ? AppColors.gray50 : .systemGray))
      
      Text(formatter.localizedString(for: article.published, relativeTo: Date()))
        .foregroundColor(Color(colorScheme == .light ? AppColors.gray50 : .systemGray))

    }
  }
  
  var body: some View {
    VStack(spacing: 5) {
      remoteImage()
      
      Spacer()

      Text(article.titleText)
        .multilineTextAlignment(.leading)
        .lineSpacing(1.0)
        .lineLimit(3)
        .truncationMode(.tail)
        .allowsTightening(true)
        .foregroundColor(Color(colorScheme == .light ? AppColors.gray90 : .white))
        .font(AppFonts.articlePreviewTitle)
        .frame(width: geometry.size.width - 40, alignment: .topLeading)
      
      Spacer()

      metaInformation()
        .frame(width: geometry.size.width - 40, alignment: .topLeading)
    }
  }
  
}
