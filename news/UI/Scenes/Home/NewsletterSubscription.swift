//
//  NewsletterSubscription.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import SwiftUI

struct NewsletterForm: View {
  
  @Environment(\.colorScheme) var colorScheme: ColorScheme
  @State private var email: String = ""
    
  var body : some View {
    
    ZStack {
      Color(self.colorScheme == .light ? AppColors.gray10 : AppColors.systemDefaultBackgroundColor).edgesIgnoringSafeArea(.all)

      Form {
        NewsletterFormHeader()
          .frame(maxWidth: .infinity, maxHeight: 10)
          .listRowBackground(Color(self.colorScheme == .light ? AppColors.gray10 : AppColors.systemDefaultBackgroundColor))

        TextField("Email", text: $email)
          .listRowBackground(Color(self.colorScheme == .light ? AppColors.gray10 : AppColors.systemDefaultBackgroundColor))
          .padding(0)
          .font(AppFonts.settingsListSubTitle)
//          .foregroundColor(Color(AppColors.gray90))
          .textFieldStyle(RoundedBorderTextFieldStyle())

        Button(action: {
          debugPrint("tap on Newsletter subscribe")
        }) {
          HStack {
            Spacer()
            Text("Subscribe")
            Spacer()
          }
        }
        .listRowBackground(Color(self.colorScheme == .light ? AppColors.gray10 : AppColors.systemDefaultBackgroundColor))
        .padding(10)
        .font(AppFonts.settingsListTitle)
        .background(Color(AppColors.buttonBackground))
        .foregroundColor(Color(self.colorScheme == .light ? AppColors.gray10 : AppColors.systemDefaultBackgroundColor))
        .cornerRadius(10)


        SwiftUIAttributedText(attributedString: AppTexts.newsletterPolicyAttributedText)
          .frame(maxWidth: .infinity, minHeight: 50, maxHeight: 50, alignment: .center)
          .padding(.horizontal, 0)
          .listRowBackground(Color(self.colorScheme == .light ? AppColors.gray10 : AppColors.systemDefaultBackgroundColor))


      }
      .background(Color(self.colorScheme == .light ? AppColors.gray10 : AppColors.systemDefaultBackgroundColor))
      .padding(.horizontal, 20)

    }
  }
}

struct NewsletterForm_Previews: PreviewProvider {
    static var previews: some View {
      NewsletterForm()
    }
}

struct NewsletterFormHeader: View {
  
  @Environment(\.colorScheme) var colorScheme: ColorScheme
  
  var body: some View {
    HStack(spacing: 10) {
      
      Text("Newsletter")
        .font(AppFonts.navigationBarTitle)

      Spacer()
      
      Button(action: {
        debugPrint("tap on close newsletter")
      }) {
        Image(systemName: "plus")
          .font(.system(size: 30))
          .rotationEffect(.degrees(45))
          .foregroundColor(Color(AppColors.gray50))
      }
      .frame(width: 20, height: 20, alignment: .center)
      .font(AppFonts.settingsListTitle)
      
    }
     .buttonStyle(PlainButtonStyle())
    
  }
}
