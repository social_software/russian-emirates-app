//
//  CategoryFilter.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import SwiftUI

struct CategoryFilter : View {
  
  var body : some View {
    
      ScrollView(.horizontal, showsIndicators: false, content: {
        HStack(spacing: 10) {
          CategoryFilterView(category: "Latest")
          CategoryFilterView(category: "World")
          CategoryFilterView(category: "Business")
          CategoryFilterView(category: "Sports")
          CategoryFilterView(category: "Life")
          CategoryFilterView(category: "Technology")
          CategoryFilterView(category: "Art")
          
//                ForEach(statuses) { status in
//                    StatusView(status: status)
//                }
          
        }
        .padding(.leading, 10)
        .padding(.trailing, 10)
      })
      .frame(height: 20)
    
  }
  
}

struct CategoryFilterView: View {
  
  @Environment(\.colorScheme) var colorScheme: ColorScheme
  
  let category: String
  
  var body: some View {
    
    Text(category)
      .foregroundColor(Color(colorScheme == .light ? AppColors.gray90 : .white))
      .font(AppFonts.categoryFilterListTitle)
    
  }
}
