//
//  Globals.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import UIKit
import SwiftUI

enum AppConstants {
    static let apiKey = "" // for the future use
}


extension AppDelegate {
    static let shared: AppDelegate = UIApplication.shared.delegate as! AppDelegate
}

enum AppColors {
  static let systemDefaultBackgroundColor = UIColor(named: "systemBackground") ?? UIColor.systemBackground
  
  static let baseColor1 = UIColor(red: 41/255, green: 103/255, blue: 1.0, alpha: 1.0)
  static let buttonBackground = baseColor1
  
  
  static let gray100 = UIColor(red: 27255, green: 27/255, blue: 29/255, alpha: 1.0)
  static let gray90 = UIColor(red: 45/255, green: 45/255, blue: 47/255, alpha: 1.0)
  
  static let gray50 = UIColor(red: 141/255, green: 141/255, blue: 142/255, alpha: 1.0)
  
  static let gray30 = UIColor(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
  
  static let gray10 = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0)
}

enum AppFonts {
  static let navigationBarTitle = Font.custom("montserrat-regular", size: UIFontMetrics(forTextStyle: .title1).scaledValue(for: 22))
  static let navigationBarTitleUI = UIFont(name: "montserrat-regular", size: UIFontMetrics(forTextStyle: .title1).scaledValue(for: 22)) ?? UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title1)
  
  static let categoryFilterListTitle = Font.custom("montserrat-regular", size: UIFontMetrics(forTextStyle: .title1).scaledValue(for: 22))
  
  static let settingsListTitle = Font.custom("montserrat-semibold", size: UIFontMetrics(forTextStyle: .headline).scaledValue(for: 18))
  static let settingsListSubTitle = Font.custom("AvenirLTStd-Medium", size: UIFontMetrics(forTextStyle: .subheadline).scaledValue(for: 18))
 
  static let settingsListSubTitleUI = UIFont(name: "AvenirLTStd-Medium", size: UIFontMetrics(forTextStyle: .subheadline).scaledValue(for: 18)) ?? UIFont.preferredFont(forTextStyle: UIFont.TextStyle.subheadline)
  
  static let articlePreviewTitle = Font.custom("montserrat-semibold", size: UIFontMetrics(forTextStyle: .title1).scaledValue(for: 21))
}

enum AppTexts {
  
  static let companyLink: String = "https://www.russianemirates.com"
  static let newsletterPolicyLink: String = "https://www.russianemirates.com"
  
  static let newsletterPolicyAttributedText: NSMutableAttributedString = {
    let text: String = "By clicking on Subscribe button you agree to accept Privacy Policy"
    let attributedString = NSMutableAttributedString(string: text, attributes: [.foregroundColor: AppColors.gray50, .font: AppFonts.settingsListSubTitleUI])

    attributedString.addAttribute(.foregroundColor, value: AppColors.buttonBackground, range: NSRange(location: 52, length: 14))
    attributedString.addAttribute(.link, value: newsletterPolicyLink, range: NSRange(location: 52, length: 14))

    return attributedString
  }()
  
}
