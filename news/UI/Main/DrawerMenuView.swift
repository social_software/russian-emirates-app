//
//  DrawerMenuView.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import SwiftUI

struct DrawerMenuContent: View {
  
    var body: some View {
      
        VStack(alignment: .leading) {
            HStack {
                Image(systemName: "person")
                    .foregroundColor(.gray)
                    .imageScale(.large)
                Text("Profile")
                    .foregroundColor(.gray)
                    .font(.headline)
            }
            .padding(.top, 100)
            HStack {
                Image(systemName: "envelope")
                    .foregroundColor(.gray)
                    .imageScale(.large)
                Text("Messages")
                    .foregroundColor(.gray)
                    .font(.headline)
            }
                .padding(.top, 30)
            HStack {
                Image(systemName: "gear")
                    .foregroundColor(.gray)
                    .imageScale(.large)
                Text("Settings")
                    .foregroundColor(.gray)
                    .font(.headline)
            }
            .padding(.top, 30)
            Spacer()
        }
            .padding()
            .frame(maxWidth: .infinity, alignment: .leading)
            .background(Color(red: 32/255, green: 32/255, blue: 32/255))
            .edgesIgnoringSafeArea(.all)
    }
  
}

struct NavigationMenuDrawer: View {
  
//    private let width = UIScreen.main.bounds.width - 100
//    let isOpen: Bool
    
//    var body: some View {
//        HStack {
//            DrawerMenuContent()
//                .frame(width: self.width)
//                .offset(x: self.isOpen ? 0 : -self.width)
//                .animation(.default)
//            Spacer()
//        }
//    }
  
  var body: some View {
    DrawerMenuContent()
  }
  
}

struct NavigationMenuDrawer_Previews: PreviewProvider {
    static var previews: some View {
        NavigationMenuDrawer()
    }
}

