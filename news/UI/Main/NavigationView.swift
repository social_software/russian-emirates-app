//
//  NavigationView.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import SwiftUI

struct AppNavigationView<Content: View >: View {
  @State var isDrawerMenuOpen: Bool = false
  @Environment(\.colorScheme) var colorScheme: ColorScheme
  
  var navigationTitle: String = "Welcome"
  var isSearchable: Bool = false
  
  var tabIndex: Int = 0
  var tabImageName: String = "home"
  
  var searchHandler: (() -> Void)? = nil
  
  let viewBuilder: () -> Content
    
  init(navigationTitle: String, isSearchable: Bool, tabIndex: Int, tabImageName:String, searchHandler: (() -> Void)? = nil, @ViewBuilder content: @escaping () -> Content) {
    self.navigationTitle = navigationTitle
    self.isSearchable = isSearchable
    self.tabIndex = tabIndex
    self.tabImageName = tabImageName
    self.searchHandler = searchHandler
    self.viewBuilder = content

    UINavigationBar.appearance().shadowImage = UIImage()
  }
    
  var body: some View {
    
    let drag = DragGesture()
        .onEnded {
            if $0.translation.width < -100 {
                withAnimation {
                    self.isDrawerMenuOpen = false
                }
            }
        }

    return NavigationView {
        GeometryReader { geometry in
          ZStack(alignment: .leading) {
            
            self.viewBuilder()
              .frame(width: geometry.size.width, height: geometry.size.height)
              .offset(x: self.isDrawerMenuOpen ? geometry.size.width/2 : 0)
              .disabled(self.isDrawerMenuOpen ? true : false)
            
            if self.isDrawerMenuOpen {
              NavigationMenuDrawer()
                .frame(width: geometry.size.width/2)
                .transition(.move(edge: .leading))
            }
            
          }
          .gesture(drag)
        }
        .navigationBarTitle(Text(navigationTitle), displayMode: .inline)
        .background(NavigationConfigurator { nc in
          nc.navigationBar.barTintColor = self.colorScheme == .light ? AppColors.systemDefaultBackgroundColor : .black
          nc.navigationBar.titleTextAttributes = [.foregroundColor : self.colorScheme == .light ? AppColors.gray90 : .white, .font: AppFonts.navigationBarTitleUI]
        })
        .navigationBarItems(leading:
            Button(action: {
              
//              debugPrint("Toast tapped!")
              
              withAnimation {
                  self.isDrawerMenuOpen.toggle()
              }
            }) { Image("toolbar-toast").renderingMode(.template).foregroundColor(self.colorScheme == .light ? Color(AppColors.gray90) : .white)}
          ,trailing: isSearchable ? AnyView(navigationBarRightView(searchHandlerInternal: {
            if self.searchHandler != nil {
              self.searchHandler!()
            }
          })) : AnyView(EmptyView())
        )
      }
      .tabItem {
        VStack {
          Image(tabImageName).renderingMode(.template)
        }
      }
      .tag(tabIndex)
  }
  
}

struct navigationBarRightView: View {
  @Environment(\.colorScheme) var colorScheme: ColorScheme
  
  var searchHandlerInternal: (() -> Void)? = nil
  
  var body: some View {
      Button(action: {
        if self.searchHandlerInternal != nil {
          self.searchHandlerInternal!()
        }
      }) { Image("toolbar-search").renderingMode(.template).foregroundColor(self.colorScheme == .light ? Color(AppColors.gray90) : .white ) }
    
  }
  
}

// Helper

struct NavigationConfigurator: UIViewControllerRepresentable {
    var configure: (UINavigationController) -> Void = { _ in }

    func makeUIViewController(context: UIViewControllerRepresentableContext<NavigationConfigurator>) -> UIViewController {
        UIViewController()
    }
    func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<NavigationConfigurator>) {
        if let nc = uiViewController.navigationController {
            self.configure(nc)
        }
    }

}
