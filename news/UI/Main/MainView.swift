//
//  MainView.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import SwiftUI

struct MainView: View {
  @EnvironmentObject var settingsStore: SettingsStore
  @EnvironmentObject var newsStore: ArticlesStore
  @EnvironmentObject var headlineNewsStore: ArticlesStore
  @EnvironmentObject var businessNewsStore: ArticlesStore
  @State private var selection = 0
  
  @Environment(\.colorScheme) var colorScheme: ColorScheme
  
  init() {
    
    UITabBar.appearance().barTintColor = colorScheme == .light ? AppColors.systemDefaultBackgroundColor : .black
    UITabBar.appearance().tintColor = colorScheme == .light ? AppColors.gray30 : .white

//    UITabBar.appearance().backgroundImage = UIImage()
    UITabBar.appearance().shadowImage = UIImage()
    UITabBar.appearance().clipsToBounds = true
  }
  
  var body: some View {
    TabView(selection: $selection) {
      TabHomeView()
      TabCategoryView()
      TabBookmarkView()
      TabProfileView()
    }
    .edgesIgnoringSafeArea(.top)
    .accentColor(self.colorScheme == .light ? Color(AppColors.baseColor1) : .white)
  }
  
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
      MainView().environmentObject(SettingsStore())
        .environmentObject(ArticlesStore(categories: Categories(), channel: .news))
        .environmentObject(ArticlesStore(categories: Categories(), channel: .headLines))
        .environmentObject(ArticlesStore(categories: Categories(), channel: .property))
    }
}


struct TabHomeView: View {
  
  @State var isShowingSearch: Bool = false
    
  var body: some View {
    AppNavigationView(navigationTitle: "Welcome", isSearchable: true, tabIndex: 0, tabImageName: "home", searchHandler: {
      debugPrint("Home search")
      self.isShowingSearch.toggle()
    }) {
      HomeView(showSearch: self.$isShowingSearch)
    }
  }
  
}

struct TabCategoryView: View {
  
  @State var isShowingSearch: Bool = false
  
  var body: some View {
    AppNavigationView(navigationTitle: "Categories", isSearchable: true, tabIndex: 1, tabImageName: "category", searchHandler: {
      debugPrint("Categories search")
      self.isShowingSearch.toggle()
    }) {
      
      Text("Categories")
        .font(.title)
    
    }
  }
  
}

struct TabBookmarkView: View {
  
  @State var isShowingSearch: Bool = false
  
  var body: some View {
    AppNavigationView(navigationTitle: "Bookmarks", isSearchable: true, tabIndex: 2, tabImageName: "bookmark", searchHandler: {
      debugPrint("Bookmarks search")
      self.isShowingSearch.toggle()
    }) {
      
      Text("Bookmarks")
        .font(.title)
    
    }
  }
  
}

struct TabProfileView: View {
  
  var body: some View {
    AppNavigationView(navigationTitle: "Profile", isSearchable: false, tabIndex: 3, tabImageName: "profile") {
      ProfileView()
    }

  }
  
}

