//
//  RemoteImage.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import SwiftUI
import Kingfisher
import UIKit

public struct RemoteImage: SwiftUI.View {

  @State private var image: UIImage? = nil

  public let imageURL: URL?
  public let placeholderImage: UIImage
  public var animation: Animation = .default
  
  public var body: some SwiftUI.View {
    Image(uiImage: image ?? placeholderImage)
      
      .renderingMode(.original)
      .resizable()
      .aspectRatio(contentMode: .fill)
      
      .onAppear(perform: loadImage)
      .transition(.opacity)
      .id(image ?? placeholderImage)
  }

  private func loadImage() {
    guard let imageURL = imageURL, image == nil else { return }
    KingfisherManager.shared.retrieveImage(with: imageURL) { result in
      switch result {
      case .success(let imageResult):
        withAnimation(self.animation) {
          self.image = imageResult.image
        }
      case .failure:
        break
      }
    }
  }
}

