//
//  SwiftUIAttributedText.swift
//  news
//
//  Created by Alex Sporyk on 28/02/2020.
//  Copyright © 2020 Russian Emirates Publishing. All rights reserved.
//

import SwiftUI

//class ViewWithTextView : UIView {
//  private var textView = UITextView()
//
//  override init(frame: CGRect) {
//    super.init(frame:frame)
//    self.addSubview(textView)
////    textView.numberOfLines = 0
//    textView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//  }
//
//  required init?(coder: NSCoder) {
//    super.init(coder: coder)
//  }
//
//  func setString(_ attributedString:NSAttributedString) {
//    self.textView.attributedText = attributedString
//  }
//}

struct SwiftUIAttributedText: UIViewRepresentable {
    
  var attributedString:NSAttributedString
  var textAlignment : NSTextAlignment? = .center
  
  func makeUIView(context: Context) -> UIAttributedTextView {
    let view = UIAttributedTextView(frame:CGRect.zero)
    return view
  }
  
  func updateUIView(_ uiView: UIAttributedTextView, context: UIViewRepresentableContext<SwiftUIAttributedText>) {
    uiView.attributedText = attributedString
    uiView.textAlignment = textAlignment ?? .center
    uiView.backgroundColor = .clear
    uiView.isEditable = false
//    uiView.isSelectable = false
    uiView.isUserInteractionEnabled = true
  }
}

struct TextWithAttributedString_Previews: PreviewProvider {
  static var previews: some View {
    SwiftUIAttributedText(attributedString: NSAttributedString(string: "Test"), textAlignment: .center)
  }
}

class UIAttributedTextView: UITextView, UITextViewDelegate {
  
  func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
      UIApplication.shared.open(URL)
      return false
  }
  
}
